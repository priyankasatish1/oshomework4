/*
Write a program that simulates a paging system using the aging algorithm. The number of page
frames is a parameter. The sequence of page references should be read from a file. For a given
input file, plot the number of page faults per 1000 memory references as a function of the number
of page frames available.

Input 1 : Number of Page frames Ex: 4
Input 2: File containing sequence of page references [0, 2, 3, 1, 0, 2, 4, 5, 5, 3, 4, 5, 0]

Result: There are 5 page faults
 */

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageFaultGenerator {
    static int NUM_FRAMES = 5;
    public static void main(String[] args) throws IOException {
        List<String> references =  getReferencesFromFile("C:\\Users\\piyak\\IdeaProjects\\oshomework4\\src\\ref1000");
        Map<Integer,CounterUtil> table = new HashMap<>();
        Integer pageFaultCount = 0;
        for(String reference : references)
        {
            System.out.println("------------------------------------------");
            System.out.println("Incoming reference request:  " + reference);
            Integer refInt = Integer.parseInt(reference);
            if(table.containsKey(refInt))
            {
                table.get(refInt).setReferenceBit((byte)1);
                System.out.println("Hit:       " + reference);
            }
            else
            {
                System.out.println("Miss:      " + refInt);
                pageFaultCount++;
                insertReference(table,refInt);
            }
            clockTick(table);
        }
        System.out.println("------------------------------------------");
        System.out.println("------------------------------------------");
        System.out.println("Total number of page faults is " + pageFaultCount);
    }

    private static void insertToTable(Map<Integer,CounterUtil> table,Integer reference)
    {
        table.put(reference, new CounterUtil());
        System.out.println("Inserting: " + reference);
    }

    private static List<String> getReferencesFromFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);

    }

    private static void  insertReference(Map<Integer,CounterUtil> table, Integer reference) {
        Byte min = (byte)255;
        if(table.size()< NUM_FRAMES)
        {
            insertToTable(table,reference);
            return;
        }
        Map.Entry<Integer, CounterUtil> minElement = null;
        for (Map.Entry<Integer, CounterUtil> entry : table.entrySet()) {
            CounterUtil counterUtil = entry.getValue();
            if (counterUtil.getCounter() == (byte) 0) {
                table.remove(entry.getKey());
                System.out.println("Evicting:  " + entry.getKey());
                insertToTable(table,reference);
                return;
            } else {
                //counterUtil.getCounter() < min
                if(compare(counterUtil.getCounter(), min))
                {
                    min = counterUtil.getCounter();
                    minElement = entry;
                }
            }
        }
        table.remove(minElement.getKey());
        System.out.println("Evicting:   " + minElement.getKey());
        insertToTable(table,reference);
    }

    // a = 10000000
    // b = 00000010
    private static Boolean compare(Byte a, Byte b)
    {
        int unsigned_a = (int)a & 0x00ff;
        int unsigned_b = (int)b & 0x00ff;
        return (unsigned_a < unsigned_b);
    }
    private static void clockTick(Map<Integer,CounterUtil> table)
    {
        for(Map.Entry<Integer,CounterUtil> entry: table.entrySet())
        {
            CounterUtil counterUtil = entry.getValue();
            counterUtil.shiftCounter();
            if(counterUtil.getReferenceBit() == 1)
            {
                counterUtil.setMSBofCounter();
                counterUtil.setReferenceBit((byte)0);
            }
        }
        printTable(table);
    }

    private static void printTable(Map<Integer,CounterUtil> table) {
        System.out.println("************** table ***************");
        for(Map.Entry<Integer,CounterUtil> entry: table.entrySet())
        {
            String binRep = Integer.toBinaryString(entry.getValue().getCounter());
            if (binRep.length() > 8) {
                binRep = binRep.substring(binRep.length() - 8);
            }
            System.out.println(String.format("%16s", entry.getKey()) + " " +
                    String.format("%16s", entry.getValue().getReferenceBit()) + " " +
                    String.format("%16s", binRep));
        }
    }
}