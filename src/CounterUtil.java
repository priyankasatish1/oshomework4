public class CounterUtil {
    Byte referenceBit = 0;
    Byte counter = 0;

    public Byte getCounter() {
        return counter;
    }

    public Byte getReferenceBit() {
        return referenceBit;
    }

    public void setCounter(Byte counter) {
        this.counter = counter;
    }

    public void setReferenceBit(Byte referenceBit) {
        this.referenceBit = referenceBit;
    }
    public void CounterUtil()
    {

    }
    public void shiftCounter()
    {
        counter = (byte)((counter & 0xff) >>> 1);
    }
    public void setMSBofCounter()
    {
        counter = (byte)(counter | (1 << 7));
    }
}

